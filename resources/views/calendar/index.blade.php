@extends('layout')

@section('content')
    <div class="jumbotron">
        <h1>Calendar</h1>

        <!-- Fetch users calendar and output on screen -->
        <calendar></calendar>

    </div>
@endsection
