@extends('layout')

@section('content')
    <div class="jumbotron">
        <h1>Tasks for this planner</h1>
        <p>Tasks marked with a <i class="fas fa-check-circle text-success"></i> are completed.</p>
        <p>Tasks marked with a <i class="fas fa-hourglass-half text-primary"></i> are in progress.</p>

        <!-- Fetch users messages and output on screen -->
        <form method="post" action="{{ url('/planner') }}">
            @csrf
            <div class="input-group my-3">
                <input type="hidden" name="plannerId" value="{{ request('planner') }}">
                <input type="text" name="title" class="form-control" placeholder="Add a task...">
                <div class="input-group-append">
                    <button type="submit" class="btn btn-outline-primary">+</button>
                </div>
            </div>
        </form>

        <ul class="list-group">
            @foreach($tasks as $task)

                <?php $taskArray[] = $task; ?>

                <li class="list-group-item">
                    {{ $task->getTitle() }}
                    @if($task->getPercentComplete() == 100)
                        <i class="fas fa-check-circle text-success"></i>
                    @elseif($task->getPercentComplete() == 50)
                        <i class="fas fa-hourglass-half text-primary"></i>
                    @endif
                </li>

            @endforeach
        </ul>
    </div>
@endsection
