@extends('layout')

@section('content')
    <div class="jumbotron">
        <h1>Planner</h1>

        <!-- Fetch users messages and output on screen -->
        <div class="list-group">
            @foreach($plans as $plan)

                <?php $planArray[] = $plan; ?>

                <a href="{{ url('/planner') }}/{{ $plan->getId() }}">{{ $plan->getTitle() }}</a>
            @endforeach
        </div>

    </div>
@endsection
