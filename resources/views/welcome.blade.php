@extends('layout')

@section('content')
    <div class="jumbotron">
        <h1>My Profile</h1>
        <p>This example shows how to get an OAuth token from Azure and to use that token to make calls to the Outlook APIs in the <a href="https://developer.microsoft.com/en-us/graph/" target="_blank">Microsoft Graph</a>.</p>
        <p>
            <a class="btn btn-lg btn-primary" href="{{ url('/signin') }}" role="button" id="connect-button">Connect to Outlook</a>
        </p>

        <mailbox></mailbox>

    </div>
@endsection
