<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Outlook REST API</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('/') }}">Home</a>
            </li>
            <li class="nav-item {{ Request::is('user') ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('/user') }}">Profile</a>
            </li>
            <li class="nav-item {{ Request::is('mail') ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('/mail') }}">Mail</a>
            </li>
            <li class="nav-item {{ Request::is('calendar') ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('/calendar') }}">Calendar</a>
            </li>
            <li class="nav-item {{ Request::is('onedrive') ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('/onedrive') }}">OneDrive</a>
            </li>
            <li class="nav-item {{ Request::is('planner') ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('/planner') }}">Planner</a>
            </li>
        </ul>

    </div>
</nav>
