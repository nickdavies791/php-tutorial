@extends('layout')

@section('content')
    <div class="jumbotron">
        <h1>OneDrive</h1>

        <!-- Fetch users messages and output on screen -->
        <div class="list-group">
            @foreach($onedrive as $item)
                <p>{{ $itemArray[] = $item->getName() }}</p>
            @endforeach
        </div>

    </div>
@endsection
