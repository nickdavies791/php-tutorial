@extends('layout')

@section('content')
    <div class="jumbotron">
        <h1>Profile: {{ $user->getDisplayName() }}</h1>
        <p>User Principal Name: {{ $user->getUserPrincipalName() }}</p>
    </div>
@endsection
