@extends('layout')

@section('content')
    <div class="jumbotron mailbox">
        <h1>Mailbox</h1>

        <mailbox></mailbox>

        <div class="card">
            <div class="card-header">
                Send Email
            </div>
            <div class="card-body">
                <form method="POST" action="{{ url('/mail/send')}}">
                    @csrf

                    <label>To:</label>
                    <input type="text" class="form-control" name="toRecipients">
                    <label>Subject</label>
                    <input type="text" class="form-control" name="subject">
                    <label>Message</label>
                    <textarea class="form-control" name="body"></textarea>

                    <button type="submit" class="btn btn-primary">Send Email</button>

                </form>
            </div>
        </div>


    </div>
@endsection
