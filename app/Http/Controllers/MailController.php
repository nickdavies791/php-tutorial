<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TokenStore\TokenCache;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;

class MailController extends Controller
{

    public function index()
    {
        return view('mail.index');
    }

    /**
     * Get Authenticated Users Mail
     */
    public function getMail()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        $tokenCache = new TokenCache;

        $graph = new Graph();
        $graph->setAccessToken($tokenCache->getAccessToken());

        $getMessagesUrl = '/me/mailFolders/inbox/messages';
        $messages = $graph->createRequest('GET', $getMessagesUrl)
                          ->setReturnType(Model\Message::class)
                          ->execute();

        return $messages;

    }

    /**
     * Send email
     */
    public function sendMail(Request $request)
    {

        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        $tokenCache = new TokenCache;

        $graph = new Graph();
        $graph->setAccessToken($tokenCache->getAccessToken());

        $user = $graph->createRequest('GET', '/me')
        ->setReturnType(Model\User::class)
        ->execute();

        $mailBody = array( "Message" => array(
                           "subject" => request('subject'),
                           "body" => array(
                               "contentType" => "html",
                               "content" => request('body'),
                           ),
                         "sender" => array(
                             "emailAddress" => array(
                                 "name" => $user->getDisplayName(),
                                 "address" => $user->getUserPrincipalName(),
                             )
                         ),
                         "from" => array(
                             "emailAddress" => array(
                                 "name" => $user->getDisplayName(),
                                 "address" => $user->getUserPrincipalName(),
                             )
                         ),
                         "toRecipients" => array(
                             array(
                               "emailAddress" => array(
                                   "address" => request('toRecipients')
                          	)
                         )
                      )
                  )
           );

        $graph->createRequest("POST", "/me/sendMail")
        	  ->attachBody($mailBody)
        	  ->execute();

    }


}
