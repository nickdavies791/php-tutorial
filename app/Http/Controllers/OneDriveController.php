<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TokenStore\TokenCache;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;

class OneDriveController extends Controller
{

    /**
     * Get Authenticated Users OneDrive Items
     */
    public function getOneDrive()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        $tokenCache = new TokenCache;

        $graph = new Graph();
        $graph->setAccessToken($tokenCache->getAccessToken());

        $onedrive = $graph->createRequest('GET', '/me/drive/root/children')
                          ->setReturnType(Model\Drive::class)
                          ->execute();

        // dd($onedrive);

        return view('onedrive.index', compact('onedrive'));

    }

}
