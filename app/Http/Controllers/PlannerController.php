<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TokenStore\TokenCache;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;

class PlannerController extends Controller
{
    public function index()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        $tokenCache = new TokenCache;

        $graph = new Graph();
        $graph->setAccessToken($tokenCache->getAccessToken());

        $getPlannerPlans = '/me/planner/plans';
        $plans = $graph->createRequest('GET', $getPlannerPlans)
                          ->setReturnType(Model\PlannerPlan::class)
                          ->execute();

        return view('planner.index', compact('plans'));
    }

    public function show(Request $request)
    {

        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        $tokenCache = new TokenCache;

        $graph = new Graph();
        $graph->setAccessToken($tokenCache->getAccessToken());

        $getPlannerTasks = '/planner/plans/'.request('planner').'/tasks';
        $tasks = $graph->createRequest('GET', $getPlannerTasks)
                          ->setReturnType(Model\PlannerTask::class)
                          ->execute();

        return view('planner.show', compact('tasks'));

    }

    public function store(Request $request)
    {

        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        $tokenCache = new TokenCache;

        $graph = new Graph();
        $graph->setAccessToken($tokenCache->getAccessToken());

        $newTask = new Model\PlannerTask();

        // dd($request);

        $newTask->setTitle($request->title);
        $newTask->setPlanId($request->plannerId);
        $newTask->setBucketId("fpuG5KZzbkKJ9djAiXoSxpYAJoSk"); // NEED TO GET BUCKET ID AND PASS HERE

        $postTask = '/planner/tasks';
        $task = $graph->createRequest('POST', $postTask)
                          ->attachBody($newTask)
                          ->setReturnType(Model\PlannerTask::class)
                          ->execute();

        return redirect('/planner/'.$request->plannerId);

    }


}
