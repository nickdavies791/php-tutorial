<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TokenStore\TokenCache;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;

class CalendarController extends Controller
{

    public function index()
    {
        return view('calendar.index');
    }

    /**
     * Get Authenticated Users Calendar Items
     */
    public function getCalendar()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        $tokenCache = new TokenCache;

        $graph = new Graph();
        $graph->setAccessToken($tokenCache->getAccessToken());

        // Return the user
        $user = $graph->createRequest('GET', '/me')
                      ->setReturnType(Model\User::class)
                      ->execute();

        $eventsQueryParams = array (
            // Only return Subject, ReceivedDateTime, and From fields
            "\$select" => "",
            // Sort by ReceivedDateTime, newest first
            "\$orderby" => "Start/DateTime DESC",
            // Return at most 10 results
            "\$top" => "100"
        );

        $getEventsUrl = '/me/events?'.http_build_query($eventsQueryParams);
        $events = $graph->createRequest('GET', $getEventsUrl)
                          ->setReturnType(Model\Event::class)
                          ->execute();

        return $events;
    }
}
