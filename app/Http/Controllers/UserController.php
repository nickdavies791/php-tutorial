<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TokenStore\TokenCache;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;

class UserController extends Controller
{

    /**
     * Get Authenticated Users Details
     */
    public function getUser()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        $tokenCache = new TokenCache;

        $graph = new Graph();
        $graph->setAccessToken($tokenCache->getAccessToken());

        // Return the user
        $user = $graph->createRequest('GET', '/me')
        ->setReturnType(Model\User::class)
        ->execute();

        // dd($user);

        return view('user.index', compact('user'));

    }

}
