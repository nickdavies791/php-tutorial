<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/signin', 'AuthController@signin');
Route::get('/authorize', 'AuthController@gettoken');

Route::get('/user', 'UserController@getUser')->name('user');

Route::get('/mail', 'MailController@index')->name('mail');
Route::get('/mail/get', 'MailController@getMail');
Route::post('/mail/send', 'MailController@sendMail');

Route::get('/calendar', 'CalendarController@index')->name('calendar');
Route::get('/calendar/get', 'CalendarController@getCalendar');

Route::resource('planner', 'PlannerController');

// Route::get('/planner', 'PlannerController@index')->name('planner');
// Route::get('/planner')
// Route::get('/planner/{id}', 'PlannerController@show');

Route::get('/onedrive', 'OneDriveController@getOneDrive')->name('onedrive');
